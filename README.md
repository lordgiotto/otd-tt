# Quick Start
Install the dependencies by running `yarn` or `npm install`.

Then run the project by running `yarn start` or `npm start`.

# Tests
Run the tests by executing `yarn test` or `npm test`.

Run and explore the tests in a web interface by executing `yarn test:ui` or `npm run test:ui`

For the scope of this test I implemented the following unit tests:
- the whole store logic in `/core/store/gridster`
- some tests for `/components/grid` component

# Notes
- This project uses create-react-app with typescript as starting point
- I have documented relevant pieces of code, and have given reasons for my choices in specific files, in the form of comments.

# PathFinding
To find the shortest path from the start to the end square of the grid I used the A* algorithm. I chose the `heap` npm package for the min priority queue.

In `/core/pathfinding` you can find the following folders:
- `grid/` contains some helpers to abstract and work with the grid and its nodes
- `astar/` contains some helpers and the function to find the path with A*

# Extra Features
For convenience, I added a faster way to clear or fill squares on the grid: click and hold the left mouse button on a square and drag the cursor to change the state of the hovered squares.

The implementation of this feature can be found in `/containers/gridster-maze/`.

# Design and structure

## Styling
The basic idea is to try to create components that resemble DOM elements: className should always be passed to the root element of the component, as well as the style object; the layout should be fluid and the style should be as minimal as possible (mainly applied to the root element to be potentially overwritten by the parent component that uses it); the visual properties should be inherited as much as possible, and variables should be used when inheritance is not possible.

## Structure
The application is divided in four main parts: components, containers, pages and core. Please find the explanation of the individual parts below.

#### Components
This folder contains the presentational components: these components hold only the presentational logic. In the vast majority of cases they should be purely functional, with some exceptions related to UI state.

For the sake of this task I created only the few components I needed. In a more complex application I usually divide them by specificity:
- **Atoms** are the smallest reasonable components and are generic enough to be used in any other project.
- **Molecules** can be either: more complex atoms, or the composition of different atoms. It's still likely they could be used in another projects.
- **Organisms** are composed of different atoms and molecules: they are usually application specific, both in layout, input props, design, etc.

I tend to prefer composition of smaller components over configuration through props of multipurpose components.

#### Containers and Pages
Containers are specific and not reusable parts of pages. Pages should be responsible for instructing the data layer/store (such as redux) of what to do based on the current route (i.e. dispatch redux actions), where containers should be responsible for getting the data from the store, and passing it to the components (and instruct the data store what to do only in case of user interaction).

#### Core
The core folder holds all the business logic of the application: it should be completely independent of the rest of the application (it uses only code written in the core itself and other npm modules). Decoupling business and presentation logic potentially allows the reusability of the same core in different apps and platforms (i.e. react web and react native).

The core folder could contain different interconnected modules, each with its responsibilities, like the redux store, the APIs, etc.
