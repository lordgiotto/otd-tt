import React, {
  CSSProperties,
  FunctionComponent,
  useState,
  useEffect,
  useCallback,
  ChangeEvent,
  FormEvent,
} from 'react'
import classnames from 'classnames'
import { connect } from 'react-redux';
import { RootState } from '../../core/store/store.type';
import { gridsterSelectors, gridsterThunks } from '../../core/store/gridster';

import Button from '../../components/button';
import Input from '../../components/input';

import style from './gridster-config.module.scss';

interface OwnProps {
  className?: string,
  style?: CSSProperties
}
interface StateProps {
  rows?: number
  cols?: number
}
interface DispatchProps {
  initMaze: (rows: number, cols: number) => void
}

type Props = OwnProps & StateProps & DispatchProps

const clampNumber = (val: number, min: number, max: number) =>
  Math.min(Math.max(val, min), max)

const GridsterConfig: FunctionComponent<Props> = ({
  className,
  style: componentStyle,
  rows,
  cols,
  initMaze,
}) => {
  const [ rowsValue, setRowsValue ] = useState<number>(10)
  const [ colsValue, setColsValue ] = useState<number>(10)
  const handleRowsChange = useCallback((e: ChangeEvent<HTMLInputElement>) => {
    const value = parseInt(e.target.value, 10)
    setRowsValue(value)
  }, [setRowsValue])
  const handleColsChange = useCallback((e: ChangeEvent<HTMLInputElement>) => {
    const value = parseInt(e.target.value, 10)
    setColsValue(value)
  }, [setColsValue])
  const handleRowBlur = useCallback((e: ChangeEvent<HTMLInputElement>) => {
    const value = parseInt(e.target.value, 10)
    const validValue = clampNumber(value, 1, 20)
    setRowsValue(validValue)
  }, [setRowsValue])
  const handleColsBlur = useCallback((e: ChangeEvent<HTMLInputElement>) => {
    const value = parseInt(e.target.value, 10)
    const validValue = clampNumber(value, 1, 20)
    setColsValue(validValue)
  }, [setColsValue])
  const handleSubmit = useCallback((e: FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    if (rowsValue && colsValue) {
      initMaze(rowsValue, colsValue)
    }
  }, [rowsValue, colsValue, initMaze])
  useEffect(() => {
    setRowsValue(rows || 10)
    setColsValue(cols || 10)
  }, [rows, cols])
  return rows && cols
    ? (
      <form
        className={classnames(
          style.gridsterConfig,
          className,
        )}
        style={componentStyle}
        onSubmit={handleSubmit}
      >
        <div className={style.sizeInputWrapper}>
          <label className={style.sizeInputLabel} htmlFor='gridster-rows'>Rows</label>
          <Input
            className={style.sizeInput}
            id='gridster-rows'
            type='number'
            min={1}
            max={20}
            value={rowsValue || ''}
            onChange={handleRowsChange}
            onBlur={handleRowBlur}
          />
        </div>
        <div className={style.inputSeparator}>
          x
        </div>
        <div className={style.sizeInputWrapper}>
          <label className={style.sizeInputLabel} htmlFor='gridster-cols'>Columns</label>
          <Input
            className={style.sizeInput}
            id='gridster-cols'
            type='number'
            min={1}
            max={20}
            value={colsValue || ''}
            onChange={handleColsChange}
            onBlur={handleColsBlur}
          />
        </div>
        <Button className={style.submitButton}>Generate</Button>
      </form>
    ) : null
}

export default connect<StateProps, DispatchProps, OwnProps, RootState>(
  state => ({
    rows: gridsterSelectors.getRows(state),
    cols: gridsterSelectors.getCols(state),
  }),
  {
    initMaze: gridsterThunks.initMaze
  }
)(GridsterConfig)
