import React, {
  useCallback,
  useRef,
  useEffect,
  MouseEvent,
  FunctionComponent,
  CSSProperties,
} from 'react'
import classnames from 'classnames'
import { gridsterActions, gridsterSelectors } from '../../core/store/gridster';
import { RootState } from '../../core/store/store.type';
import { connect } from 'react-redux';
import Grid from '../../components/grid'

import style from './gridster-maze.module.scss';

interface OwnProps {
  className?: string,
  style?: CSSProperties
}
interface StateProps {
  maze?: number[][]
  startPos?: [number, number]
  endPos?: [number, number]
  path: [number, number][]
}
interface DispatchProps {
  setCellValue: (row: number, col: number, value: number) => void
}

type Props = OwnProps & StateProps & DispatchProps

const isSameCell =
  (pos1: [number, number], pos2: [number, number]) =>
    pos1[0] === pos2[0]
      && pos1[1] === pos2[1]

const GridsterMaze: FunctionComponent<Props> = ({
  className,
  style: componentStyle,
  maze,
  startPos,
  endPos,
  path,
  setCellValue,
}) => {
  // The following code enables the change of a tile's state both by clicking
  // and by dragging the cursor (better UX if the maze is big)
  const fastActionRef = useRef<number | undefined>()
  const handleCellMouseDown = useCallback((_, row: number, col: number) => {
    if (maze) {
      const isStart = !!startPos && isSameCell(startPos, [row, col])
      const isEnd = !!endPos && isSameCell(endPos, [row, col])
      const prevValue = maze[row][col]
      const newValue =
        prevValue === 0 || isStart || isEnd
          ? 1
          : 0
      fastActionRef.current = newValue
      if (!isStart && !isEnd) {
          setCellValue(row, col, newValue)
      }
    }
  }, [maze, startPos, endPos, setCellValue])
  const handleCellMouseEnter = useCallback((event: MouseEvent<HTMLElement>, selectedRow: number, selectedCol: number) => {
    if (event.buttons === 1 && typeof fastActionRef.current !== 'undefined') {
      setCellValue(selectedRow, selectedCol, fastActionRef.current)
    }
  }, [setCellValue])
  useEffect(() => {
    const resetFastAction = () => fastActionRef.current = undefined
    document.addEventListener('mouseup', resetFastAction)
    return () =>
      document.removeEventListener('mouseup', resetFastAction)
  }, [])
  return maze && startPos && endPos
    ? (
      <Grid
        className={classnames(
          style.gridsterMaze,
          className
        )}
        style={componentStyle}
        model={maze}
        start={startPos}
        end={endPos}
        path={path}
        onCellMouseDown={handleCellMouseDown}
        onCellMouseEnter={handleCellMouseEnter}
      />
    )
    : null
}

export default connect<StateProps, DispatchProps, OwnProps, RootState>(
  state => ({
    maze: gridsterSelectors.getMaze(state),
    startPos: gridsterSelectors.getStart(state),
    endPos: gridsterSelectors.getEnd(state),
    path: gridsterSelectors.getPath(state)
  }),
  {
    setCellValue: (row: number, col: number, value: number) =>
      gridsterActions.setCellValue({
        cell: [row, col],
        value
      })
  }
)(GridsterMaze);
