import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import storeReducer from './store.reducer'
import storeMiddlewares from './store.middleware'

const composeEnhancers: typeof compose =
  (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

export default
  createStore(
    storeReducer,
    composeEnhancers(
      applyMiddleware(thunk, ...storeMiddlewares)
    )
  )
