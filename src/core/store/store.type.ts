import { Action as FsaAction } from 'typescript-fsa'
import { Dispatch } from 'redux'
import { ThunkDispatch } from 'redux-thunk'

import { State as GridsterState } from './gridster'

export interface Action extends FsaAction<any> {
  type: any
}
export type Dispatch = ThunkDispatch<RootState, any, Action>
export type StateGetter = () => RootState
export type Store = {
  dispatch: Dispatch
  getState: StateGetter,
}
export type Middleware = (
  store: Store
) => (next: Dispatch) => (action: Action) => Action | Promise<Action>

export interface RootState {
  gridster: GridsterState
}
