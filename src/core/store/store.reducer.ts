import { combineReducers } from 'redux'
import { Action, RootState } from './store.type'

import gridsterReducer from './gridster'

const storeReducer = combineReducers<RootState, Action>({
  gridster: gridsterReducer
})

export default storeReducer
