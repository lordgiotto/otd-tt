import * as randomHelpers from '../../helpers/random'
import * as actions from './gridster.action'
import * as thunks from './gridster.thunk'

describe('Redux > Gridster > Thunks', () => {
  describe('initMaze()', () => {
    let dispatch: jest.Mock
    let generateRandomNumberSpy: jest.SpyInstance
    beforeAll(() => {
      generateRandomNumberSpy =jest.spyOn(randomHelpers, 'generateRandomNumber')
      dispatch = jest.fn()
    })
    afterAll(() => {
      jest.restoreAllMocks()
    })
    it('should generate the maze with a random start on the first column and a random end on the last column', () => {
      generateRandomNumberSpy.mockReturnValueOnce(3)
      generateRandomNumberSpy.mockReturnValueOnce(7)
      thunks.initMaze(7, 10)(dispatch)
      const expectedAction = actions.generateMaze({
        rows: 7,
        cols: 10,
        start: [3, 0],
        end: [7, 9]
      })
      expect(dispatch.mock.calls[0][0]).toEqual(expectedAction)
    })
  })
})
