import reducer, { initialState } from './gridster.reducer'
import * as actions from './gridster.action'
import { State } from './gridster.type'

describe('Redux > Gridster > Reducer', () => {
  it('should have the correct initial state', () => {
    const newState = reducer(undefined, { type: 'not existing action' })
    expect(newState).toEqual(initialState)
  })
  describe('on reset', () => {
    it('should reset the state to the initalState', () => {
      const newState = reducer(initialState, actions.reset())
      expect(newState).toEqual(initialState)
    })
  })
  describe('on generateMaze', () => {
    const action = actions.generateMaze({
      rows: 5,
      cols: 5,
      start: [3, 0],
      end: [0, 4],
    })
    it('should set "rows" and "cols" as provided', () => {
      const newState = reducer(initialState, action)
      expect(newState.rows).toEqual(5)
      expect(newState.cols).toEqual(5)
    })
    it('should set "maze" as a correct size matrix, where start and end have cost of 1', () => {
      const expectedMaze = [
        [0, 0, 0, 0, 1],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [1, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
      ]
      const newState = reducer(initialState, action)
      expect(newState.maze).toEqual(expectedMaze)
    })
    it('should set "start" and "end" as the coordinate provided', () => {
      const newState = reducer(initialState, action)
      expect(newState.start).toEqual([3, 0])
      expect(newState.end).toEqual([0, 4])
    })
  })
  describe('on setCellValue', () => {
    const action = actions.setCellValue({
      cell: [1, 1],
      value: 7
    })
    it('should have no effect if the maze is not defined', () => {
      const prevState = {
        maze: undefined
      } as State
      const newState = reducer(prevState, action)
      expect(newState).toEqual(prevState)
    })
    it('should set the provided value at the [row, col] position of the matrix', () => {
      const prevState = {
        maze: [
          [0, 0, 1, 0],
          [1, 0, 1, 0],
          [1, 1, 1, 0],
        ]
      } as State
      const expectedState = {
        maze: [
          [0, 0, 1, 0],
          [1, 7, 1, 0],
          [1, 1, 1, 0],
        ]
      } as State
      const newState = reducer(prevState, action)
      expect(newState).toEqual(expectedState)
    })
  })
})
