import { generateRandomNumber } from '../../helpers/random'
import { Dispatch } from '../store.type'
import { generateMaze } from './gridster.action'

// I decided to use a thunk to init the maze for the following reasons:
// - ideally the reducer function should be pure and deterministic, and generate
//   random numbers there would have led to a lot of problem in unit-testing it
// - typescript-fsa action creators are not intended to provide typesafe actions,
//   not to include any logic
// - the connected page should not know about and should not have the responsability of
//   generate the random start and end position
export const initMaze = (rows: number = 10, cols: number = 10) =>
  (dispatch: Dispatch) => {
    const startRow = generateRandomNumber(0, rows - 1)
    const endRow = generateRandomNumber(0, rows - 1)
    dispatch(generateMaze({
      rows,
      cols,
      start: [startRow, 0],
      end: [endRow, cols - 1],
    }))
  }
