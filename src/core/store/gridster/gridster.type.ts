export type GridCell = [number, number]

export interface State {
  rows?: number,
  cols?: number,
  maze?: number[][]
  start?: GridCell
  end?: GridCell
}
