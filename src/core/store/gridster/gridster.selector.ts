import { createSelector } from 'reselect'
import aStarFindPath from '../../pathfinding/astar'
import { RootState } from '../store.type'
import { State } from './gridster.type'

export const getModuleState = (state: RootState): State => state.gridster
export const getRows = (state: RootState) => getModuleState(state).rows
export const getCols = (state: RootState) => getModuleState(state).cols
export const getMaze = (state: RootState) => getModuleState(state).maze
export const getStart = (state: RootState) => getModuleState(state).start
export const getEnd = (state: RootState) => getModuleState(state).end
// The path depends on the maze, start and end position: I don't need to store
// it in the store. I calculate it on the fly only if one of that values changes.
// I use reselect to create a memoized selector that is recalculated only if
// either maze, start or end changes.
export const getPath = createSelector(
  getMaze,
  getStart,
  getEnd,
  (maze, start, end) =>
    maze && start && end
      ? aStarFindPath(maze, start, end)
          .map(node => node.pos)
      : []
)


