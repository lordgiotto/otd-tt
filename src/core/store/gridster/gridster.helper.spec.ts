import { createMazeMatrix } from './gridster.helper'

describe('Redux > Gridster > Helpers', () => {
  describe('createMazeMatrix', () => {
    it('should return a matrix with specified rows and cols', () => {
      const rows = 10
      const cols = 7
      const matrix = createMazeMatrix(rows, cols)
      expect(matrix).toHaveLength(rows)
      matrix.forEach(row => expect(row).toHaveLength(cols))
    })
    it('should set every value of the matrix to the defaultValue', () => {
      const defaultValue = 7
      const matrix = createMazeMatrix(4, 3, defaultValue)
      expect(matrix).toEqual([
        [defaultValue, defaultValue, defaultValue],
        [defaultValue, defaultValue, defaultValue],
        [defaultValue, defaultValue, defaultValue],
        [defaultValue, defaultValue, defaultValue],
      ])
    })
    it('should set defaultValue to 0 if not provided', () => {
      const matrix = createMazeMatrix(4, 3)
      expect(matrix).toEqual([
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0],
      ])
    })
  })
})
