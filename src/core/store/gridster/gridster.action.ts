import { actionCreatorFactory } from 'typescript-fsa'
import { GridCell } from './gridster.type'

// These actions creators don't need any unit test: they don't contain any logic
// and their output is safe enought because of typescript
const actionCreator = actionCreatorFactory('gridster')

export const reset = actionCreator<void>('RESET')
export const generateMaze = actionCreator<{
  rows: number,
  cols: number,
  start: GridCell,
  end: GridCell,
}>('MAZE_GENERATE')
export const setCellValue = actionCreator<{
  cell: GridCell,
  value: number,
}>('CELL_VALUE_SET')
