import { reducerWithInitialState } from 'typescript-fsa-reducers'
import * as actions from './gridster.action'
import { State } from './gridster.type'
import { createMazeMatrix } from './gridster.helper'

export const initialState: Readonly<State> = {
  rows: undefined,
  cols: undefined,
  maze: undefined,
  start: undefined,
  end: undefined,
}

export default reducerWithInitialState(initialState)
  .case(actions.reset, () => initialState)
  .case(actions.generateMaze, (state, { rows, cols, start, end }) => ({
    ...state,
    rows,
    cols,
    start,
    end,
    maze:
      createMazeMatrix(rows, cols, 0)
        .map((row, rowIndex) =>
          row.map((cost, colIndex) =>
            (
              (rowIndex === start[0] && colIndex === start[1])
                || (rowIndex === end[0] && colIndex === end[1])
            ) ? 1 : cost
          )
        )
  }))
  .case(actions.setCellValue, (state, { cell, value }) => ({
    ...state,
    maze: state.maze
      ? state.maze.map((row, rowIndex) =>
        row.map((prevValue, colIndex) =>
          (rowIndex === cell[0] && colIndex === cell[1])
            ? value
            : prevValue
        )
      )
      : undefined
  }))
