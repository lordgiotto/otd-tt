// Generates a matrix with specified rows and cols, and fills
// every element of the array with the defaultValue (default 0)
export const createMazeMatrix =
  (rows: number, cols: number, defaultValue: number = 0) =>
    [...Array(rows)].map(() => Array(cols).fill(defaultValue))
