import { RootState } from '../store.type'
import * as selectors from './gridster.selector'

const mockState = {
  gridster: {
    rows: 3,
    cols: 3,
    maze: [
      [1, 0, 0],
      [0, 0, 0],
      [0, 0, 1],
    ],
    start: [0, 0],
    end: [2, 2],
  }
} as RootState
const mockStateMazeOk = {
  ...mockState,
  gridster: {
    ...mockState.gridster,
    maze: [
      [1, 1, 0],
      [0, 1, 0],
      [0, 1, 1],
    ]
  }
}
const mockStateNoMaze = {
  ...mockState,
  gridster: {
    ...mockState.gridster,
    maze: undefined
  }
}
const mockStateNoStart = {
  ...mockState,
  gridster: {
    ...mockState.gridster,
    start: undefined
  }
}
const mockStateNoEnd = {
  ...mockState,
  gridster: {
    ...mockState.gridster,
    end: undefined
  }
}

describe('Redux > Gridster > Selectors', () => {
  describe('getRows()', () => {
    it('should return the number of rows', () => {
      expect(selectors.getRows(mockState)).toBe(mockState.gridster.rows)
    })
  })
  describe('getCols()', () => {
    it('should return the number of columns', () => {
      expect(selectors.getCols(mockState)).toBe(mockState.gridster.cols)
    })
  })
  describe('getMaze()', () => {
    it('should return the maze matrix', () => {
      expect(selectors.getMaze(mockState)).toBe(mockState.gridster.maze)
    })
  })
  describe('getStart()', () => {
    it('should return the start coordinates', () => {
      expect(selectors.getStart(mockState)).toBe(mockState.gridster.start)
    })
  })
  describe('getEnd()', () => {
    it('should return the end coordinates', () => {
      expect(selectors.getEnd(mockState)).toBe(mockState.gridster.end)
    })
  })
  describe('getPath()', () => {
    it('should return an empty array if either maze, start or end are undefined', () => {
      expect(selectors.getPath(mockStateNoMaze)).toEqual([])
      expect(selectors.getPath(mockStateNoStart)).toEqual([])
      expect(selectors.getPath(mockStateNoEnd)).toEqual([])
    })
    it('should return an empty array if the maze has no connection between start and end', () => {
      expect(selectors.getPath(mockState)).toEqual([])
    })
    it('should return an array of points if the maze has some connection between start and end', () => {
      expect(selectors.getPath(mockStateMazeOk)).not.toHaveLength(0)
    })
    it('should be recalculated only if either maze, start or end changes', () => {
      const path1 = selectors.getPath(mockStateMazeOk)
      const path2 = selectors.getPath(mockStateMazeOk)
      const path3 = selectors.getPath(mockState)
      expect(path1).toBe(path2)
      expect(path1).not.toBe(path3)
    })
  })
})
