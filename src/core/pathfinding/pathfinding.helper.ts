export const isInRange = (val: number, min: number, max: number) =>
  val >= min && val <= max
