import Heap from 'heap'
import { OpenGridNode, NodesTrace } from './pathfinding.astar.type'
import { isSameNode, GridNode } from '../grid'

export const getOpenNode =
  (node: GridNode, prevCost: number, heuristic: number): OpenGridNode => {
    const costFromStart = prevCost + node.cost
    const costToEnd = heuristic
    return {
      ...node,
      costFromStart,
      costToEnd,
      priority: costFromStart + costToEnd
    }
  }

export const getMinPriorityQueue =
  <T extends OpenGridNode>() =>
    new Heap<T>((a, b) => a.priority - b.priority)

export const backtrace =
  (trace: NodesTrace, startNode: GridNode, endNode: GridNode) => {
    const path: GridNode[] = []
    let found = false
    let parentNode: GridNode | undefined = endNode
    while (parentNode) {
      path.unshift(parentNode)
      if (isSameNode(parentNode, startNode)) {
        found = true
        break
      }
      parentNode = trace[parentNode.id]
    }
    return found ? path : []
  }
