import {
  getNode,
  manhattanHeuristic,
  getNeighbors,
  isSameNode,
  Grid,
  GridPoint,
} from '../grid'
import {
  getMinPriorityQueue,
  getOpenNode,
  backtrace,
} from './pathfinding.astar.helper'
import { NodesTrace } from './pathfinding.astar.type'

const aStarFindPath =
  (matrix: Grid, start: GridPoint, end: GridPoint) => {
    const flood = getMinPriorityQueue()
    const startNode = getNode(matrix, start)
    const endNode = getNode(matrix, end)
    if (!startNode || !endNode) {
      return []
    }
    const openedStartNode =
      getOpenNode(startNode, 0, manhattanHeuristic(startNode, endNode))
    flood.push(openedStartNode)
    const nodesPriority: { [nodeId: string]: number | undefined } = {
      [startNode.id]: openedStartNode.priority,
    }
    const trace: NodesTrace = {}
    while (!flood.empty()) {
      const currentNode = flood.pop()!
      const neighbors = getNeighbors(matrix, currentNode.pos)
      if (isSameNode(currentNode, endNode)) {
        break
      }
      for (let i = 0; i < neighbors.length; i++) {
        const nextNode = neighbors[i]
        const openedNextNode = getOpenNode(
          nextNode,
          currentNode.costFromStart,
          manhattanHeuristic(nextNode, endNode)
        )
        const prevPriority = nodesPriority[openedNextNode.id]
        if (
          typeof prevPriority === 'undefined'
          || openedNextNode.priority < prevPriority
        ) {
          nodesPriority[openedNextNode.id] = openedNextNode.priority
          trace[openedNextNode.id] = currentNode
          flood.push(openedNextNode)
        }
      }
    }
    return backtrace(trace, startNode, endNode)
  }

export default aStarFindPath
