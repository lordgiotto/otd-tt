import { GridNode } from '../grid'

export interface OpenGridNode extends GridNode {
  costFromStart: number,
  costToEnd: number,
  priority: number
}
export type NodesTrace = {
  [nodeId: string]: GridNode
}
