import { isInRange } from '../pathfinding.helper'
import { Grid, GridPoint, GridNode } from './pathfinding.grid.type'
import { NEIGHBORS_SIMPLE } from './pathfinding.grid.const'

export const getNode =
  (matrix: Grid, [row, col]: GridPoint): GridNode | undefined => {
    const inRange =
      matrix[row] &&
      isInRange(row, 0, matrix.length - 1) &&
      isInRange(col, 0, matrix[row].length - 1)
    return inRange ? {
      id: `${row}-${col}`,
      pos: [row, col],
      cost: matrix[row][col]
    } : undefined
  }

export const getNeighbors =
  (matrix: Grid, [row, col]: GridPoint): GridNode[] =>
    NEIGHBORS_SIMPLE
      .map(([rowMov, colMov]) =>
        getNode(matrix, [row + rowMov, col + colMov])
      )
      .filter(
        (cell): cell is NonNullable<typeof cell> =>
          !!cell && !!cell.cost
      )

export const isSameNode =
  (node1: GridNode, node2: GridNode) =>
    node1.pos[0] === node2.pos[0]
      && node1.pos[1] === node2.pos[1]

export const manhattanHeuristic =
  (node1: GridNode, node2: GridNode) => {
    var rowD = Math.abs(node2.pos[0] - node1.pos[0])
    var colD = Math.abs(node2.pos[1] - node1.pos[1])
    return rowD + colD
  }
