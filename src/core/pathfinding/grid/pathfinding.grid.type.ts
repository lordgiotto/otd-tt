export type Grid = number[][]
export type GridPoint = [number, number]

export interface GridNode {
  id: string
  pos: GridPoint
  cost: number
}
