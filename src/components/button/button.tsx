import React, {
  CSSProperties,
  FunctionComponent,
  ReactNode,
  ButtonHTMLAttributes,
} from 'react'
import classnames from 'classnames'

import style from './button.module.scss'

interface Props extends ButtonHTMLAttributes<HTMLButtonElement> {
  className?: string,
  style?: CSSProperties,
  children?: ReactNode,
}

const Button: FunctionComponent<Props> = ({
  className,
  style: componentStyle,
  children,
  ...otherProps
}) => (
  <button
    className={classnames(
      style.button,
      className,
    )}
    style={componentStyle}
    {...otherProps}
  >
    {children}
  </button>
)

export default Button
