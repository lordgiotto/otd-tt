import React, {
  FunctionComponent,
  CSSProperties,
  useMemo,
  MouseEvent,
} from 'react'
import classnames from 'classnames'
import Tile, { TileStatus } from '../tile';

import style from './grid.module.scss'

interface Props {
  className?: string,
  style?: CSSProperties
  model: number[][]
  start: [number, number]
  end: [number, number]
  path: [number, number][]
  onCellClick?: (event: MouseEvent<HTMLDivElement>, row: number, col: number) => void
  onCellMouseDown?: (event: MouseEvent<HTMLDivElement>, row: number, col: number) => void
  onCellMouseEnter?: (event: MouseEvent<HTMLDivElement>, row: number, col: number) => void
}

const createTileStatusGetter = ({
  start,
  end,
  path,
}: {
  start: [number, number],
  end: [number, number],
  path: [number, number][],
}) =>
  (cell: number, row: number, col: number): TileStatus => {
    if (start[0] === row && start[1] === col) {
      return 'start'
    } else if (end[0] === row && end[1] === col) {
      return 'end'
    } else if (path.find(item => item[0] === row && item[1] === col)) {
      return 'path'
    } else if (cell === 0) {
      return 'filled'
    } else {
      return 'clear'
    }
  }

const Grid: FunctionComponent<Props> = ({
  className,
  style: componentStyle,
  model,
  start,
  end,
  path,
  onCellClick = () => {},
  onCellMouseDown = () => {},
  onCellMouseEnter = () => {},
}) => {
  const getTileStatus = useMemo(() => createTileStatusGetter({start, end, path}), [start, end, path])
  return (
    <div
      className={classnames(style.grid, className)}
      style={componentStyle}
    >
      {model.map((row, rowIndex) => (
        <div key={`grid-row-${rowIndex}`} className={style.row}>
          {row.map((cell, colIndex) => (
            <Tile
              className={style.cell}
              key={`grid-cell-${rowIndex}-${colIndex}`}
              status={getTileStatus(cell, rowIndex, colIndex)}
              onClick={event => onCellClick(event, rowIndex, colIndex)}
              onMouseDown={event => onCellMouseDown(event, rowIndex, colIndex)}
              onMouseEnter={event => onCellMouseEnter(event, rowIndex, colIndex)}
            />
          ))}
        </div>
      ))}
    </div>
  )
}

export default Grid
