import React from 'react'
import { shallow } from 'enzyme'
import Grid from './grid'
import Tile from '../tile'

import style from './grid.module.scss'

const maze = [
  [1, 1, 0, 1],
  [1, 1, 0, 1],
  [0, 1, 0, 1],
  [1, 1, 1, 1],
]
const start: [number, number] = [1, 0]
const end: [number, number] = [3, 3]
const path: [number, number][] = [
  [1, 0],
  [1, 1],
  [2, 1],
  [3, 1],
  [3, 2],
  [3, 3],
]

describe('components > Grid', () => {
  it('should render', () => {
    const component = shallow(<Grid model={maze} start={start} end={end} path={path} />)
    expect(component.exists()).toBe(true)
  })
  it('should match the snapshot', () => {
    const component = shallow(<Grid model={maze} start={start} end={end} path={path} />)
    expect(component).toMatchSnapshot()
  })
  it('should add the provided className to the root element', () => {
    const component = shallow(<Grid className='aClass' model={maze} start={start} end={end} path={path} />)
    expect(component.first().hasClass('aClass')).toBe(true)
  })
  it('should contain the correct numbers of rows', () => {
    const component = shallow(<Grid model={maze} start={start} end={end} path={path} />)
    expect(component.find(`.${style.row}`)).toHaveLength(4)
  })
  it('should contain the correct numbers of Tile elements', () => {
    const component = shallow(<Grid model={maze} start={start} end={end} path={path} />)
    expect(component.find(Tile)).toHaveLength(16)
  })
})
