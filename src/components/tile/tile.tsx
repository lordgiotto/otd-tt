import React, { FunctionComponent, CSSProperties, DOMAttributes } from 'react'
import classnames from 'classnames'

import style from './tile.module.scss'

type RootElementProps = DOMAttributes<HTMLDivElement>
type InheritedPropNames =
  | 'onClick'
  | 'onMouseEnter'
  | 'onMouseDown'

export type TileStatus = 'filled' | 'clear' | 'start' | 'end' | 'path'

interface Props extends Pick<RootElementProps, InheritedPropNames> {
  className?: string,
  status?: TileStatus
  style?: CSSProperties
}

const Tile: FunctionComponent<Props> = ({
  className,
  style: componentStyle,
  status,
  ...otherProps
}) => (
  <div
    className={classnames(
      style.tile,
      status && style[`status-${status}`],
      className
    )}
    style={componentStyle}
    {...otherProps}
  >
    <div className={style.inner} />
  </div>
)

export default Tile
