import React, {
  CSSProperties,
  FunctionComponent,
  InputHTMLAttributes,
} from 'react'
import classnames from 'classnames'

import style from './input.module.scss'

interface Props extends InputHTMLAttributes<HTMLInputElement> {
  className?: string,
  style?: CSSProperties,
}

const Input: FunctionComponent<Props> = ({
  className,
  style: componentStyle,
  ...otherProps
}) => (
  <input
    className={classnames(
      style.input,
      className,
    )}
    style={componentStyle}
    {...otherProps}
  />
)

export default Input
