import React, { CSSProperties, FunctionComponent, useEffect } from 'react'
import { connect } from 'react-redux'
import classnames from 'classnames'
import { RootState } from '../../core/store/store.type';
import { gridsterThunks } from '../../core/store/gridster';

import GridsterConfig from '../../containers/gridster-config';
import GridsterMaze from '../../containers/gridster-maze'

import logo from '../../assets/gridster-logo.png'

import style from './gridster.module.scss'

interface OwnProps {
  className?: string,
  style?: CSSProperties
}
interface StateProps {}
interface DispatchProps {
  initMaze: () => void
}

type Props = OwnProps & StateProps & DispatchProps

const GridsterPage: FunctionComponent<Props> = ({
  className,
  style: componentStyle,
  initMaze
}) => {
  useEffect(() => {
    initMaze()
  }, [initMaze])
  return (
    <div
      className={classnames(
        style.gridster,
        className
      )}
      style={componentStyle}
    >
      <img className={style.logo} src={logo} alt='Gridster' />
      <GridsterConfig className={style.configPanel} />
      <GridsterMaze className={style.mazeBox} />
    </div>
  )
}

export default connect<StateProps, DispatchProps, OwnProps, RootState>(
  undefined,
  {
    initMaze: gridsterThunks.initMaze
  }
)(GridsterPage)
