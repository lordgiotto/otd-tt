import React, { FunctionComponent } from 'react'
import { Provider } from 'react-redux'
import store from './core/store'

import GridsterPage from './pages/gridster'

const App: FunctionComponent = () => (
  <Provider store={store}>
    <GridsterPage />
  </Provider>
)

export default App
